---
title: 'My first Webslides'
---
<!--: .wrap .size-70 ..aligncenter -->

## **Hugo-Webslides**

<!--: .text-intro -->Use markdown to write contents and render with [**WebSlides**](https://webslides.tv) to HTML.

{{< svg fa-camera >}}
